package com.smartfoxpro.simplerestservice.controllers;

import com.smartfoxpro.simplerestservice.dto.CustomerDto;
import com.smartfoxpro.simplerestservice.dto.CustomerUpdateDto;
import com.smartfoxpro.simplerestservice.entity.Customer;
import com.smartfoxpro.simplerestservice.services.CustomerService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/customers")
public class CustomerController {

    private final CustomerService customerService;

    @GetMapping
    public List<Customer> customers() {
        return customerService.findAll();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Customer> getById(@PathVariable Long id) {
       return customerService.findById(id).map(customer -> ResponseEntity.ok(customer)).orElseGet(() -> ResponseEntity.noContent().build());
    }

    @PostMapping
    public Customer createNewCustomer(@Valid @RequestBody Customer customer) {
        return customerService.save(customer);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Customer> update(@PathVariable Long id, @Valid @RequestBody Customer customer) {
        return customerService.update(id, customer).map(c -> ResponseEntity.ok(c)).orElseGet(() -> ResponseEntity.badRequest().build());
    }

    @PatchMapping(value = "/{id}")
    public ResponseEntity<CustomerDto> updateCustomer(@PathVariable Long id, @RequestBody CustomerUpdateDto customerUpdateDto) {
        return customerService.updateCustomerLoginAndPassword(id, customerUpdateDto).map(c -> ResponseEntity.ok(c)).orElseGet(() -> ResponseEntity.badRequest().build());
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.HEAD)
    public ResponseEntity<Customer> getUserStatus(@PathVariable Long id) {
        return customerService.findById(id).map(customer -> ResponseEntity.ok(customer)).orElseGet(() -> ResponseEntity.badRequest().build());
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Customer> delete(@PathVariable Long id) {
        return customerService.delete(id).map(customer -> ResponseEntity.ok(customer)).orElseGet(() -> ResponseEntity.notFound().build());
    }

}
