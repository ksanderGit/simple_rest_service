package com.smartfoxpro.simplerestservice.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class CustomerUpdateDto {
    @NotBlank
    private String login;
    @NotBlank
    private String password;
}
