package com.smartfoxpro.simplerestservice.services;

import com.smartfoxpro.simplerestservice.dto.CustomerDto;
import com.smartfoxpro.simplerestservice.dto.CustomerUpdateDto;
import com.smartfoxpro.simplerestservice.entity.Customer;
import com.smartfoxpro.simplerestservice.repositories.CustomerRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class CustomerServiceImpl implements CustomerService {
    private final CustomerRepository repository;
    private final ModelMapper modelMapper;

    @Override
    public List<Customer> findAll() {
        return repository.findAll();
    }

    @Override
    public Optional<Customer> findById(Long id) {
        return repository.findById(id);
    }

    @Override
    public Customer save(Customer customer) {
        modelMapper.map(customer, Customer.class);
        return repository.save(customer);
    }

    @Override
    public Optional<Customer> update(Long id, Customer customer) {
        Customer updatedCustomer = modelMapper.map(customer, Customer.class);
        updatedCustomer.setId(id);
        return Optional.ofNullable(updatedCustomer);
    }

    @Override
    public Optional<CustomerDto> updateCustomerLoginAndPassword(Long id, CustomerUpdateDto customerUpdateDto) {
        Optional<Customer> customerOpt = repository.findById(id);
        if (customerOpt.isEmpty()) {
            return Optional.empty();
        }else {
            Customer customer = customerOpt.get();
            modelMapper.map(customerUpdateDto, customer);
            return Optional.of(modelMapper.map(customer, CustomerDto.class));
        }
    }

    @Override
    public Optional<Customer> delete(Long id) {
        Optional<Customer> customer = repository.findById(id);
        if (customer.isPresent()) {
            repository.deleteById(id);
            return customer;
        }else {
            return Optional.empty();
        }
    }
}
