package com.smartfoxpro.simplerestservice.services;

import com.smartfoxpro.simplerestservice.dto.CustomerDto;
import com.smartfoxpro.simplerestservice.dto.CustomerUpdateDto;
import com.smartfoxpro.simplerestservice.entity.Customer;

import java.util.List;
import java.util.Optional;

public interface CustomerService {
    List<Customer> findAll();

    Optional<Customer> findById(Long id);

    Customer save(Customer customer);

    Optional<Customer> update(Long id, Customer customer);

    Optional<Customer> delete(Long id);

    Optional<CustomerDto> updateCustomerLoginAndPassword(Long id, CustomerUpdateDto customerUpdateDto);
}
